<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
		<title>员工签到</title>
		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<!--basic styles-->
		<link href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<%=request.getContextPath()%>/assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-fonts.css" />
		<!--ace styles-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-skins.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/sign.css" />
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-ie.min.css" />
		<![endif]-->
		<style type="text/css">
			span
            {
                line-height:2.5;
            }
		</style>
	</head>
	<body class="login-layout">
			<div class="main-container container-fluid">
				<div class="main-content">
							<div class="login-container">
								<div class="row-fluid">
									<div class="position-relative">
										<div id="login-box" class="login-box visible widget-box no-border">
											<div class="widget-body">
												<div class="widget-main">
													<div class="row-fluid">
														<div class="center">
															<h1 style="padding-left: 26px;padding-right: 26px;">
																<span class="red" style="cursor: pointer;">
																	<img alt="" src="assets/images/btn_qiandao2.png"/>
																</span>
																<c:if test="${score>=100 }">
																<span class="red" style="cursor: pointer;" onclick="choujiang(true)">
																	<img alt="" src="assets/images/btn_qiandao6.png"/>
																</span>
																</c:if>
																<c:if test="${score<100 }">
																<span class="red" style="cursor: pointer;" onclick="choujiang(false)">
																	<img alt="" src="assets/images/btn_qiandao3.png"/>
																</span>
																</c:if>
															</h1>
														</div>
													</div>
													<div class="row-fluid">
														<div class="center">
															<h1 style="padding-left: 26px;padding-right: 26px;">
																<c:if test="${isqiandao==true }">
																	<span class="red" style="cursor: pointer;">
																		<img alt="" src="assets/images/btn_qiandao4.png"/>
																	</span>
																</c:if>
																<c:if test="${isqiandao!=true }">
																	<span class="red" style="cursor: pointer;" onclick="qiandaoclk()">
																		<img alt="" src="assets/images/btn_qiandao1.png"/>
																	</span>
																</c:if>
															</h1>
														</div>
													</div>
													<div class="row-fluid">
														<div class="center" style="font-family:'微软雅黑';">
															<table  style="width: 60%;margin:auto">
															<tr style="color:#ffb700; font-size:18px;text-shadow:0 0 2px #fbcf63;"><td>${score }</td><td>${days }</td></tr>
															<tr style=" font-size:14px;text-shadow:0 0 2px #aaaaaa;"><td>可用积分</td><td>签到天数</td></tr>
															</table>
														</div>
													</div>
													<div class="row-fluid">
														<div style="margin:0 auto; width:300px;" id="calendar">
														</div>
													</div>
													</div>
												</div><!--/widget-main-->
											</div><!--/widget-body-->
										</div><!--/login-box-->
	
									</div><!--/position-relative-->
								</div>
								
								<!-- Modal -->
							<div id="myModal" class="modal hide fade" tabindex="-1" style="-webkit-border-radius: 40px;border-radius: 40px;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  	<img alt="" src="assets/images/btn_qiandaowarn.png">
							</div>
							</div>
				</div>
			<!--basic scripts-->
	
			<!--[if !IE]>-->
	
			<script type="text/javascript">
				window.jQuery || document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
			</script>
			<!--<![endif]-->
	
			<!--[if IE]>
			<script type="text/javascript">
			 window.jQuery || document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
			</script>
			<![endif]-->
	
			<script type="text/javascript">
				if("ontouchend" in document) document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
			</script>
			<script src="<%=request.getContextPath()%>/assets/js/bootstrap.min.js"></script>
	
			<!--page specific plugin scripts-->
	
			<!--ace scripts-->
	
			<script src="<%=request.getContextPath()%>/assets/js/ace-elements.min.js"></script>
			<script src="<%=request.getContextPath()%>/assets/js/ace.min.js"></script>
			<script src="<%=request.getContextPath()%>/assets/js/calendar.js"></script>
			<script type="text/javascript">
			$(function(){
				 //ajax获取日历json数据
				 var signList=eval('${list}');
				 calUtil.init(signList);
			});
			function qiandaoclk(){//点击签到按钮
				document.write("<form action='<%=request.getContextPath()%>/qiandao' method=post name=formx2 style='display:none'>");
			    document.write("<input type=hidden name='userid' value='${student.id}'/>");
				document.write("</form>");
				document.formx2.submit(); 
			}
			function prevFun(){
		        var month = calUtil.showMonth-1;
			    var year = calUtil.showYear;
		        if(month==0){
		            month=12;
		            year-=1;
		        }
				$.ajax({
			       url:'<%=request.getContextPath()%>/qiandaodays', //后台处理程序
			       type:'post',         //数据发送方式
			       dataType:'json',     //接受数据格式
			       data:{year:year,month:month},         //要传递的数据
			       success:function(data){//回传函数(这里是函数名)
			   		   calUtil.eventName="prev";
			   		   calUtil.init(data.list);
			       } 
			    });
			}
			function nextFun(){
				var month = calUtil.showMonth+1;
			    var year = calUtil.showYear;
		        if(month==13){
		            month=1;
		            year+=1;
		        }
				$.ajax({
			       url:'<%=request.getContextPath()%>/qiandaodays', //后台处理程序
			       type:'post',         //数据发送方式
			       dataType:'json',     //接受数据格式
			       data:{year:year,month:month},         //要传递的数据
			       success:function(data){//回传函数(这里是函数名)
			   		   calUtil.eventName="next";
			   		   calUtil.init(data.list);
			       } 
			     });
			}
			function choujiang(flag){
				if(!flag){
					$('#myModal').modal();
				}else{
					document.write("<form action='<%=request.getContextPath()%>/choujiangpage' method=post name=formx1 style='display:none'>");
				    document.write("<input type=hidden name='userid' value='${student.id}'/>");
					document.write("</form>");
					document.formx1.submit();
				}
			}
			</script>
		</body>
</html>