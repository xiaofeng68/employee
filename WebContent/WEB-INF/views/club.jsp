<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>俱乐部信息</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@include file="/WEB-INF/views/common/css.jsp" %>
	</head>
	<body>
		<%@ include file="/WEB-INF/views/common/navbar.jsp" %>
		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>
			<%@ include file="/WEB-INF/views/common/sidebar.jsp" %>
			<div class="main-content">
				 
				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							<small>
								<i class="icon-list-alt"></i>
								俱乐部信息
							</small>
						</h1>
					</div> 
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<form class="form-inline" id="searchClubForm" method="post"  action="<%=request.getContextPath() %>/manager/club">
								<input type="hidden" id="pagenum" name="pagenum" value="${pagenum}">
								&nbsp;&nbsp;名称：<input type="text" name="name" value="${club.name}"  class="input-medium search-query">&nbsp;&nbsp;&nbsp;&nbsp;
								<button  type="submit" class="btn btn-purple btn-small">
									查找
									<i class="icon-search icon-on-right bigger-110"></i>
								</button>
								<button  type="button" class="btn btn-purple btn-small" onclick="location.href='<%=request.getContextPath() %>/manager/club'" >
									清空
									<i class="icon-remove icon-on-right bigger-110"></i>
								</button>
								<button  type="button" onclick="location.href='<%=request.getContextPath() %>/manager/addclubpage'" class="btn btn-purple btn-small">
									添加俱乐部
									<i class="icon-plus-sign icon-on-right bigger-110"></i>
								</button>
							</form>
							
							<table id="sample-table-1" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th width="10%">俱乐部编号</th>
										<th width="20%">俱乐部名称</th>
										<th width="10%">待核实成员</th>
										<th width="10%">俱乐部成员</th>
										<th width="20%">俱乐部描述</th>
										<th width="40%" >操作</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${grid.rows}"  var="club"  >
									<tr>
										<td>${club.id}</td>
										<td>${club.name}</td>
										<td>${club.outstatus }</td>
										<td>${club.instatus }</td>
										<td>${club.des}</td>
										<td >
											<c:if test="${club.status==1 }">
												<button class="btn btn-mini btn-primary" onclick="location.href='<%=request.getContextPath() %>/manager/clubchangestate?id=${club.id}&status=0'" ><i class=" icon-ok"></i>&nbsp;开放</button>
											</c:if>
											<c:if test="${club.status==0 }">
												<button class="btn btn-mini btn-primary" onclick="if(window.confirm('确认关闭俱乐部${club.name}？')==true)location.href='<%=request.getContextPath() %>/manager/clubchangestate?id=${club.id}&status=1'" ><i class="icon-remove"></i>&nbsp;关闭</button>
											</c:if>
											<button class="btn btn-mini btn-primary" onclick="location.href='<%=request.getContextPath() %>/manager/studentclubmanagerpage?clubid=${club.id}'" ><i class="icon-user"></i>&nbsp;查看申请</button>
											<button class="btn btn-mini btn-primary" onclick="location.href='<%=request.getContextPath() %>/manager/studentclubmanagerpage?clubid=${club.id}&status=2'" ><i class="icon-user"></i>&nbsp;俱乐部成员</button>
											<a href="#myModal"  role="button" onclick="setvalue('${club.id}','${club.name}','${club.des}')" class="btn  btn-mini btn-info" data-toggle="modal"><i class="icon-edit"></i>编辑</a>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
					 		<div class="dataTables_paginate paging_bootstrap pagination">
							  <c:if test="${grid.pages > 1}">
							  <button class="btn btn-success btn-mini" onclick="searchClub(1)">首页</button>
							  <select name="classid" id="editclassid" class="btn btn-success input-medium" onchange="searchClub(this.value)">
							  	<c:forEach  varStatus="num" begin="1" end="${grid.pages }">
									<option value="${num.index }" <c:if test="${num.index ==grid.currentPage }"> selected="selected"</c:if>>第${num.index }页</option>
								</c:forEach>
							  </select>
							  <button class="btn btn-success btn-mini" onclick="searchClub(${grid.pages })" >末页</button>
							 </c:if>
					 		</div>
					 		<!-- Modal -->
							<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  	<form action="<%=request.getContextPath() %>/manager/updateclub" id="updateclub" method="post"  class="form-inline">
								  <div class="modal-header">
								    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								    <h3 id="myModalLabel">编辑俱乐部</h3>
								  </div>
								  <div class="modal-body">
								  		<input type="hidden"  name="id"  id="editid" >
										<label class="control-label"   >俱乐部名称:&nbsp;&nbsp;</label>
										<input type="text" name="name" class="input-medium"  id="editname"  >
										<br><br><label class="control-label"    >俱乐部描述:&nbsp;&nbsp;</label>
										<input type="text" name="club" id="editdes"  class="input-xlarge" >
								  </div>
								  <div class="modal-footer">
								    <button  type="button" id="modify" class="btn btn-small btn-primary">更新</button>
								  </div>
							  	</form>
							</div>
							<!--PAGE CONTENT ENDS-->
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div><!--/.page-content-->
		</div><!--/.main-content-->
	</div><!--/.main-container-->
	<%@include file="/WEB-INF/views/common/js.jsp" %>
	<script type="text/javascript">
		$(function() {
			$('#modify').on('click', function() {
				if($.trim($("#editname").val())==''){
					alert('请输入俱乐部名称！');
					return;
				}else{
					$("#updateclub").submit();
				}
			});
		});
		function setvalue(id,name,remark){
			$("#editid").val(id);
			$("#editname").val(name);
			$("#editdes").val(remark);
		}
		function searchClub(pagenum){
			$("#pagenum").val(pagenum);
			$("#searchClubForm").submit();
		}
	</script>
	</body>
</html>