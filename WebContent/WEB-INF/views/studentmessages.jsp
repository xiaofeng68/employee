<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>微信接收信息</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@include file="/WEB-INF/views/common/css.jsp" %>
	</head>

	<body>
		<%@ include file="/WEB-INF/views/common/navbar.jsp" %>
		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>
			<%@ include file="/WEB-INF/views/common/sidebar.jsp" %>
			<div class="main-content">
				 
				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							<small>
								<i class="icon-comment"></i>
								员工反馈消息
							</small>
						</h1>
					</div> 
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							 <!--PAGE CONTENT BEGINS-->
							<form class="form-inline" method="post" id="searchStudentMessage" action="<%=request.getContextPath() %>/manager/studentmessages">
								<input type="hidden" name="pagenum" id="pagenum" value="${pagenum}"/>
								&nbsp;&nbsp;员工编号：
								<input type="text" name="studentid" value="${studentMessage.studentid==0?'':studentMessage.studentid }"  class="input-small search-query"/>&nbsp;&nbsp;
								&nbsp;&nbsp;员工姓名：
								<input type="text" name="sname" value="${studentMessage.sname}"  class="input-small search-query"/>&nbsp;&nbsp;
								<select name="classid" class="input-medium">
									<option value="0">选择公司</option>
									<c:forEach items="${clsList}"  var="cls"  >
										<option <c:if test="${studentMessage.classid == cls.id}">selected="selected"</c:if> value="${cls.id}">${cls.name}</option>
									</c:forEach>
								</select>&nbsp;&nbsp;反馈信息：
								<input type="text" name="content" value="${studentMessage.content}"  class="input-small search-query"/>&nbsp;&nbsp;
								<button  type="submit" class="btn btn-purple btn-small">
									查找
									<i class="icon-search icon-on-right bigger-110"></i>
								</button>
								<button  type="button" class="btn btn-purple btn-small" onclick="location.href='<%=request.getContextPath() %>/manager/studentmessages'" >
									清空
									<i class="icon-remove icon-on-right bigger-110"></i>
								</button>
							</form>
							
							<c:if test="${param.notice != null}">
								<div class="alert alert-info">
									<button type="button" class="close" data-dismiss="alert">
										<i class="icon-remove"></i>
									</button>
									<i class="icon-ok"></i>
									<strong>${param.notice}</strong>
								</div>
							</c:if>
							<hr>
							<table id="sample-table-1" class="table table-striped table-bordered table-hover">
								<thead>
									<tr >
								      <th width="5%">消息编号</th>
								      <th width="10%">员工姓名</th>
								      <th width="10%">所在公司</th>
								      <th>反馈信息</th>
								      <th width="15%">反馈时间</th>
								      <!-- <th>回复信息</th>
								      <th width="15%">回复时间</th> -->
								      <th width="15%">操作</th>
								    </tr>
								  </thead>
								  <tbody>
								  	<c:forEach items="${grid.rows}"  var="message"   varStatus="st" >
								  		<tr>
								  		<td>${message.id}</td>
								  		<td>${message.sname}</td>
								  		<td>${message.cname}</td>
								  		<td>${message.content}</td>
								  		<td><fmt:formatDate value="${message.inserttime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
								  		<%-- <td>${message.bkcontent}</td>
								  		<td><fmt:formatDate value="${message.updatetime}" pattern="yyyy-MM-dd HH:mm:ss"/></td> --%>
								  		<td>
											<%-- <a href="#myModal"  role="button" onclick="setvalue('${message.id}','${message.sname}')" class="btn  btn-mini btn-info" data-toggle="modal"><i class="icon-edit"></i>回复</a> --%>
											<button class="btn btn-mini btn-danger" onclick="if(window.confirm('确认删除员工${student.sname}反馈信息？')==true)location.href='<%=request.getContextPath() %>/manager/deletestudentmessage?id=${message.id}'"><i class="icon-remove"></i>移除</button>
										</td>
								  		</tr>
								  	</c:forEach>
								  </tbody>
							</table>
							
					 		<div class="dataTables_paginate paging_bootstrap pagination">
							  <c:if test="${grid.pages > 1}">
							  <button class="btn btn-success btn-mini" onclick="searchStudentMessage(1)">首页</button>
							  <select name="classid" id="editclassid" class="btn btn-success input-medium" onchange="searchStudentMessage(this.value)">
							  	<c:forEach  varStatus="num" begin="1" end="${grid.pages }">
									<option value="${num.index }" <c:if test="${num.index ==grid.currentPage }"> selected="selected"</c:if>>第${num.index }页</option>
								</c:forEach>
							  </select>
							  <button class="btn btn-success btn-mini" onclick="searchStudentMessage(${grid.pages })" >末页</button>
							 </c:if>
					 		</div>
							 <!-- Modal -->
							<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  	<form action="<%=request.getContextPath() %>/manager/updatestudentmessage" id="updatestudentmessage" method="post"  class="form-inline">
								  <div class="modal-header">
								    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								    <h3 id="myModalLabel"></h3>
								  </div>
								  <div class="modal-body">
								  		<input type="hidden"  name="id"  id="editid" >
										<label class="control-label"   >内容:&nbsp;&nbsp;</label>
										<textarea class="span12" name="bkcontent" id="editbkcontent" rows="4" placeholder="回复内容"></textarea>
								  </div>
								  <div class="modal-footer">
								    <button  type="button" id="modify" class="btn btn-small btn-primary">回复</button>
								  </div>
							  	</form>
							</div>
							<!--PAGE CONTENT ENDS-->
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div><!--/.page-content-->
		</div><!--/.main-content-->
	</div><!--/.main-container-->

		<%@include file="/WEB-INF/views/common/js.jsp" %>
		<script type="text/javascript">
			$(function() {
				$('#modify').on('click', function() {
					if($.trim($("#editbkcontent").val())==''){
						alert('请输入回复信息！');
						return;
					}else{
						$("#updatestudentmessage").submit();
					}
				});
			});
			function setvalue(id,name){
				$("#editid").val(id);
				$("#myModalLabel").val("回复("+name+")");
			}
			function searchStudentMessage(pagenum){
				$("#pagenum").val(pagenum);
				$("#searchStudentMessage").submit();
			}
		</script>
	</body>
</html>