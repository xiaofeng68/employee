<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
		<title>员工申请</title>
		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<!--basic styles-->
		<link href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<%=request.getContextPath()%>/assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-fonts.css" />
		<!--ace styles-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-ie.min.css" />
		<![endif]-->
		<style type="text/css">
			span
            {
                line-height:2.5;
            }
		</style>
	</head>
	<body class="login-layout">
			<div class="main-container container-fluid">
				<div class="main-content">
							<div class="login-container">
								<div class="row-fluid">
									<div class="position-relative">
										<div id="login-box" class="login-box visible widget-box no-border">
											<div class="widget-body">
												<div class="widget-main">
													<div class="space-20"></div>
													<!-- logo -->
													<div class="row-fluid">
														<div class="center">
															<h1>
																<span class="red"><img alt="" src="assets/images/LOGO.png"/></span>
																<span class="white"></span>
															</h1>
														</div>
													</div>
													<label class="center"> 
														<b>请填写以下信息</b>
													</label>
	
													<form action="<%=request.getContextPath()%>/julebusq" id="shenqing" method="post" >
														
														<fieldset>
															<label style="background-color: #FFFFFF;border-top:1px solid #dadada;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;姓名&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	${student.name }
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-remove-circle"></i>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;性别&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	<select id="jlbid" name="jlbid" class="input-medium" style="-webkit-appearance:listitem;-moz-appearance:listitem;border:0;outline:none;-webkit-box-shadow: 0 0 0px 1000px white inset; ">
																		<option value="">请选择</option>
																		<option value="1">男</option>
																		<option value="0">女</option>
																	</select>
																	<i class="icon-chevron-down" onclick="clearTxt(2)"></i>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;公司&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	${student.className }
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-chevron-down"></i>
																</span>
															</label>
															<label  style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right ">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;工号&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	${student.id }
																	<input type="hidden" class="input-medium"  id="studentid" name="studentid" value="${student.id }" />
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-remove-circle" ></i>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;短号&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	${student.phone }-${student.remark}
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-remove-circle" ></i>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;俱乐部&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	<input type="hidden" value="${club.id }" name="clubid"/>
																	${club.name}
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-chevron-down"></i>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-center">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;备注&nbsp;&nbsp;&nbsp;&nbsp;</b>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block text-center">
																	<textarea style="width:95%" name="content" id="content" rows="4" ></textarea>
																</span>
															</label>
															<div class="row-fluid">
																<div class="center">
																	<h1 style="padding-left: 26px;padding-right: 26px;">
																		<span class="red" style="cursor: pointer;" onclick="checkValidate()"><img alt="" src="assets/images/btn_submit.png"/></span>
																	</h1>
																</div>
															</div>
															<div class="center">
															${notice }
															</div>
															<div class="space-4">
															</div>
														</fieldset>
													</form>
													
												</div><!--/widget-main-->
											</div><!--/widget-body-->
										</div><!--/login-box-->
	
									</div><!--/position-relative-->
								</div>
							</div>
				</div>
			</div><!--/.main-container-->
			<!--basic scripts-->
	
			<!--[if !IE]>-->
	
			<script type="text/javascript">
				window.jQuery || document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
			</script>
			<!--<![endif]-->
	
			<!--[if IE]>
			<script type="text/javascript">
			 window.jQuery || document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
			</script>
			<![endif]-->
	
			<script type="text/javascript">
				if("ontouchend" in document) document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
			</script>
			<script src="<%=request.getContextPath()%>/assets/js/bootstrap.min.js"></script>
	
			<!--page specific plugin scripts-->
	
			<!--ace scripts-->
	
			<script src="<%=request.getContextPath()%>/assets/js/ace-elements.min.js"></script>
			<script src="<%=request.getContextPath()%>/assets/js/ace.min.js"></script>
			<script type="text/javascript">
				function clearTxt(type){
					if(type==2){
						open($("#classid"));
					}
				}
				function checkValidate(){
					if(!$("#content").val()){
						return alert("请输入留言信息！");
						return false;
					}
					document.getElementById('shenqing').submit();
				}
				function open(elem) {
				    if (document.createEvent) {
				        var e = document.createEvent("MouseEvents");
				        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
				        elem[0].dispatchEvent(e);
				    } else if (element.fireEvent) {
				        elem[0].fireEvent("onmousedown");
				    }
				}
			</script>
		</body>
</html>