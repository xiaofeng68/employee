<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>员工信息</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<%@include file="/WEB-INF/views/common/css.jsp" %>
	</head>

	<body>
		<%@ include file="/WEB-INF/views/common/navbar.jsp" %>
		<div class="main-container container-fluid">
			<a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>
			<%@ include file="/WEB-INF/views/common/sidebar.jsp" %>
			<div class="main-content">
				 
				<div class="page-content">
					<div class="page-header position-relative">
						<h1>
							<small>
								<i class="icon-user"></i>
								员工信息
							</small>
						</h1>
					</div> 
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<form class="form-inline" id="searchStudentForm" method="post" action="<%=request.getContextPath() %>/manager/students">
								<input type="hidden" id="pagenum" name="pagenum" value="1"/>
								&nbsp;&nbsp;员工编号：
								<input type="text" name="id" value="${studentId }"  class="input-small search-query"/>&nbsp;&nbsp;
								姓名：
								<input type="text" name="name" value="${student.name}"  class="input-small search-query"/>&nbsp;&nbsp;
								短号：
								<input type="text" name="remark" value="${student.remark}"  class="input-small search-query"/>&nbsp;&nbsp;
								<select name="classid" class="input-medium">
									<option value="0">所在公司</option>
									<c:forEach items="${clsList}"  var="cls"  >
										<option <c:if test="${student.classid == cls.id}">selected="selected"</c:if> value="${cls.id}">${cls.name}</option>
									</c:forEach>
								</select>&nbsp;&nbsp;
								<button  type="submit" class="btn btn-purple btn-small">
									查找
									<i class="icon-search icon-on-right bigger-110"></i>
								</button>
								<button  type="button" class="btn btn-purple btn-small" onclick="location.href='<%=request.getContextPath() %>/manager/students'" >
									清空
									<i class="icon-remove icon-on-right bigger-110"></i>
								</button>
								<a href="#myModalAdd"  role="button"  class="btn btn-purple btn-small" data-toggle="modal"><i class="icon-plus-sign icon-on-right bigger-110"></i>添加员工</a>
							</form>
							
							<c:if test="${param.notice != null}">
								<div class="alert alert-info">
									<button type="button" class="close" data-dismiss="alert">
										<i class="icon-remove"></i>
									</button>
									<i class="icon-ok"></i>
									<strong>${param.notice}</strong>
								</div>
							</c:if>
							<hr>
							<table id="sample-table-1" class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th width="8%">员工编号</th>
										<th width="8%">员工姓名</th>
										<th width="15%">所属公司</th>
										<th>短号</th>
										<th>手机号</th>
										<th width="15%">微信标示</th>
										<th width="15%">更新时间</th>
										<th width="15%" >操作</th>
									</tr>
								</thead>
								<tbody>
								<c:forEach items="${grid.rows}"  var="student"  >
									<tr>
										<td><a href="#">${student.id}</a></td>
										<td>${student.name}</td>
										<td>${student.className}</td>
										<td>${student.remark}</td>
										<td>${student.phone}</td>
										<td>${student.openid}</td>
										<td><fmt:formatDate value="${student.updatetime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
										<td >
											<a href="#myModal"  role="button" onclick="setvalue('${student.classid }','${student.id}','${student.name}','${student.remark}','${student.phone}','${student.openid }')" class="btn  btn-mini btn-info" data-toggle="modal"><i class="icon-edit"></i>编辑</a>
											<button class="btn btn-mini btn-danger" onclick="if(window.confirm('确认删除员工${student.name}？')==true)location.href='<%=request.getContextPath() %>/manager/deletestudent?studentid=${student.id}&classid=${student.classid}&type=1'"><i class="icon-remove"></i>删除</button>
										</td>
									</tr>
								</c:forEach>
								</tbody>
							</table>
							
					 		<div class="dataTables_paginate paging_bootstrap pagination">
					 		<c:if test="${grid.pages > 1}">
							  <button class="btn btn-success btn-mini" onclick="searchStudent(1)">首页</button>
							  <select name="classid"  class="btn btn-success input-medium" onchange="searchStudent(this.value)">
							  	<c:forEach  varStatus="num" begin="1" end="${grid.pages }">
									<option value="${num.index }" <c:if test="${num.index ==grid.currentPage }"> selected="selected"</c:if>>第${num.index }页</option>
								</c:forEach>
							  </select>
							  <button class="btn btn-success btn-mini" onclick="searchStudent(${grid.pages })" >末页</button>
							 </c:if>
					 		</div>
					 		<!-- Modal -->
							<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  	<form action="updatestudent" id="updatestudent" method="post"  class="form-inline">
								  <div class="modal-header">
								    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								    <h3 id="myModalLabel">编辑员工</h3>
								  </div>
								  <div class="modal-body">
								  		<input type="hidden" name="viewName" value="/manager/students"/>
								  		<input type="hidden"  name="id"  id="editid" >
								  		<input type="hidden"  name="openid"  id="editopenid" >
										<label class="control-label"   >姓名:&nbsp;&nbsp;</label>
										<input type="text" name="name" class="input-medium"  id="editname"  />
										<label class="control-label"   >公司:&nbsp;&nbsp;</label>
										<select name="classid" id="editclassid" class="input-medium" >
											<option value="0">选择公司</option>
											<c:forEach items="${clsList}"  var="cls"  >
												<option  value="${cls.id}">${cls.name}</option>
											</c:forEach>
										</select>&nbsp;&nbsp;
										<br><br><label class="control-label"    >短号:&nbsp;&nbsp;</label>
										<input type="text" name="remark" id="editremark"  class="input-medium" >
										<label class="control-label"    >手机号:</label>
										<input type="text" name="phone" id="editphone" class="input-medium"   placeholder="手机号"/>
								  </div>
								  <div class="modal-footer">
								    <button  type="button" id="modify" class="btn btn-small btn-primary">更新</button>
								  </div>
							  	</form>
							</div>
							<div id="myModalAdd" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							<form  class="form-inline" id="savestudent" method="post" action="addstudent" class="form-inline">
								  <div class="modal-header">
								    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								    <h3 id="myModalLabel">添加员工</h3>
								  </div>
								  <div class="modal-body">
								  	<input type="hidden" name="viewName" value="/manager/students"/>
									<label class="control-label"  >编号:&nbsp;&nbsp;</label>
									<input type="text" id="id" name="id" placeholder="编号"/>
									<label class="control-label"   >姓名:&nbsp;&nbsp;</label>
									<input type="text" name="name" id="name"  placeholder="姓名"/>
									&nbsp;&nbsp;
									<br><label class="control-label"   >公司:&nbsp;&nbsp;</label>
									<select name="classid" id="classid">
										<option value="0">选择公司</option>
										<c:forEach items="${clsList}"  var="cls"  >
											<option  value="${cls.id}">${cls.name}</option>
										</c:forEach>
									</select>
									<label class="control-label"    >短号:&nbsp;&nbsp;</label>
									<input type="text" name="remark" id="remark"    placeholder="短号"/>
									&nbsp;&nbsp;
									<br>
									<label class="control-label"    >手机号:</label>
									<input type="text" name="phone" id="phone"    placeholder="手机号"/>
								  </div>
								  <div class="modal-footer">
								    <button  type="button" id="addstudent" class="btn btn-small btn-primary">添加</button>
								  </div>
							</form>
							</div>
							<!--PAGE CONTENT ENDS-->
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div><!--/.page-content-->
		</div><!--/.main-content-->
	</div><!--/.main-container-->

		<%@include file="/WEB-INF/views/common/js.jsp" %>
		<script type="text/javascript">
		$(function() {
			$('#addstudent').on('click', function() {
				var id = parseInt($.trim($("#id").val()));
				if(isNaN(id)){
					alert('员工编号为数字且为必填项！');
					return;
				}else if($.trim($("#name").val())==''){
					alert('请输入员工姓名！');
					return;
				}else if('0'==$("#classid").val()){
					alert('请选择公司！');
					return;
				}else if($.trim($("#phone").val())!=''){//非空时验证有效性
					var isPhone = /^([0-9]{3,4}-)?[0-9]{7,8}$/;
				    var isMob=/^((\+?86)|(\(\+86\)))?(13[012356789][0-9]{8}|15[012356789][0-9]{8}|18[02356789][0-9]{8}|147[0-9]{8}|1349[0-9]{7})$/;
			        var value=$.trim($("#phone").val());
			        if(!isMob.test(value)&&!isPhone.test(value)){
			        	alert("请输入正确的电话号码，\n\n如：0531-88888888，18678809957");
			            return;
			        }
				}
				
				$("#savestudent").submit();
			});
			$('#modify').on('click', function() {
				if($.trim($("#editname").val())==''){
					alert('请输入员工姓名！');
					return;
				}else if($.trim($("#editphone").val())!=''){//非空时验证有效性
					var isPhone = /^([0-9]{3,4}-)?[0-9]{7,8}$/;
				    var isMob=/^((\+?86)|(\(\+86\)))?(13[012356789][0-9]{8}|15[012356789][0-9]{8}|18[02356789][0-9]{8}|147[0-9]{8}|1349[0-9]{7})$/;
			        var value=$.trim($("#editphone").val());
			        if(!isMob.test(value)&&!isPhone.test(value)){
			        	alert("请输入正确的电话号码，\n\n如：0531-88888888，18678809957");
			            return;
			        }
				}
				$("#updatestudent").submit();
			});
		});
		
		function setvalue(clsid,id,name,remark,phone,openid){
			$("#editclassid").val(clsid);
			$("#editid").val(id);
			$("#editname").val(name);
			$("#editremark").val(remark);
			$("#editphone").val(phone);
			$("#editopenid").val(openid);
		}
		function searchStudent(pagenum){
			$("#pagenum").val(pagenum);
			$("#searchStudentForm").submit();
		}
		</script>
	</body>
</html>