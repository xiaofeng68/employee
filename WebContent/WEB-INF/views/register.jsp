<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
		<title>员工注册</title>
		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
		<!--basic styles-->
		<link href="<%=request.getContextPath()%>/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="<%=request.getContextPath()%>/assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-fonts.css" />
		<!--ace styles-->
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-responsive.min.css" />
		<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/ace-ie.min.css" />
		<![endif]-->
	</head>
	<body class="login-layout">
			<div class="main-container container-fluid">
				<div class="main-content">
							<div class="login-container">
								<div class="row-fluid">
									<div class="position-relative">
										<div id="login-box" class="login-box visible widget-box no-border">
											<div class="widget-body">
												<div class="widget-main">
													<br/>
													<!-- title -->
													<!-- logo -->
													<div class="row-fluid">
														<div class="center">
															<h1>
																<span class="red"><img alt="" src="assets/images/LOGO.png"/></span>
																<span class="white"></span>
															</h1>
														</div>
													</div>
													<div class="space-20"></div>
	
													<form action="<%=request.getContextPath()%>/register/studentAdd" method="post" id="login" >
														
														<fieldset>
															<label  style="background-color: #FFFFFF;border-top:1px solid #dadada;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right ">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;工号&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	<input type="text" class="input-medium"  id="id" name="id"  placeholder="请输入工号" style="border:0;outline:none;-webkit-box-shadow: 0 0 0px 1000px white inset; "/>
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-remove-circle" onclick="clearTxt(1)"></i>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;公司&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	<select id="classid" name="classid" class="input-medium" style="-webkit-appearance:listitem;-moz-appearance:listitem;border:0;outline:none;-webkit-box-shadow: 0 0 0px 1000px white inset; ">
																		<option value="0">选择公司</option>
																		<c:forEach items="${clsList}"  var="cls"  >
																			<option value="${cls.id}">${cls.name}</option>
																		</c:forEach>
																	</select>
																	<i class="icon-chevron-down" onclick="clearTxt(2)"></i>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;姓名&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	<input type="text" class="input-medium"  id="name" name="name" placeholder="请输入姓名" style="border:0;outline:none;-webkit-box-shadow: 0 0 0px 1000px white inset; "/>
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-remove-circle" onclick="clearTxt(3)"></i>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;" >
																<span class="block input-icon input-icon-right">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;短号&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	<input type="text" class="input-medium"  id="remark" name="remark" placeholder="请输入公司短号" style="border:0;outline:none;-webkit-box-shadow: 0 0 0px 1000px white inset; "/>
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-remove-circle" onclick="clearTxt(4)"></i>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;">
																<span class="block input-icon input-icon-right">
																	<b>&nbsp;&nbsp;&nbsp;&nbsp;密码&nbsp;&nbsp;&nbsp;&nbsp;</b>
																	<input type="password" class="input-medium"  id="password" name="password" placeholder="请输入密码" style="border:0;outline:none;-webkit-box-shadow: 0 0 0px 1000px white inset; "/>
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-remove-circle" onclick="clearTxt(5)"></i>
																</span>
															</label>
															<label style="background-color: #FFFFFF;border-bottom:1px solid #dadada;margin-bottom:0px;">
																<span class="block input-icon input-icon-right">
																	<b>确认密码</b>
																	<input type="password" class="input-medium"  id="repassword" placeholder="请输入确认密码" style="border:0;outline:none;-webkit-box-shadow: 0 0 0px 1000px white inset; "/>
																	&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-remove-circle" onclick="clearTxt(5)"></i>
																</span>
															</label>
															<div class="space"></div>
		
															<div class="row-fluid">
																<div class="center">
																	<h1 style="padding-left: 26px;padding-right: 26px;">
																		<span class="red" style="cursor: pointer;" onclick="checkValidate()"><img alt="" src="assets/images/btn_regedit.png"/></span>
																	</h1>
																</div>
															</div>
															<div class="center">
															${notice }
															</div>
															<div class="space-4">
															</div>
														</fieldset>
														<input type="hidden" name="openid" value="${openid }"/>
													</form>
													
												</div><!--/widget-main-->
											</div><!--/widget-body-->
										</div><!--/login-box-->
	
									</div><!--/position-relative-->
								</div>
							</div>
				</div>
			</div><!--/.main-container-->
			<!--basic scripts-->
	
			<!--[if !IE]>-->
	
			<script type="text/javascript">
				window.jQuery || document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
			</script>
			<!--<![endif]-->
	
			<!--[if IE]>
			<script type="text/javascript">
			 window.jQuery || document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
			</script>
			<![endif]-->
	
			<script type="text/javascript">
				if("ontouchend" in document) document.write("<script src='<%=request.getContextPath()%>/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
			</script>
			<script src="<%=request.getContextPath()%>/assets/js/bootstrap.min.js"></script>
	
			<!--page specific plugin scripts-->
	
			<!--ace scripts-->
	
			<script src="<%=request.getContextPath()%>/assets/js/ace-elements.min.js"></script>
			<script src="<%=request.getContextPath()%>/assets/js/ace.min.js"></script>
			<script type="text/javascript">
				function clearTxt(type){
					if(type==1){//清空名称
						$("#id").val("");
					}else if(type==2){
						open($("#classid"));
					}else if(type==3){
						$("#name").val("");
					}else if(type==4){
						$("#remark").val("");
					}else if(type==5){
						$("#password").val("");
					}
				}
				function open(elem) {
				    if (document.createEvent) {
				        var e = document.createEvent("MouseEvents");
				        e.initMouseEvent("mousedown", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
				        elem[0].dispatchEvent(e);
				    } else if (element.fireEvent) {
				        elem[0].fireEvent("onmousedown");
				    }
				}
				function checkValidate(){
					if(!$("#id").val()){
						return alert("请输入工号！");
						return false;
					}
					if(isNaN(parseInt($("#id").val()))){
						return alert("请输入数字工号！");
						return false;
					}
					if(!$("#classid").val()){
						alert("请选择公司！");
						return false;
					}
					if(!$("#name").val()){
						return alert("请输入姓名！");
						return false;
					}
					if($("#remark").val()){
						if(isNaN(parseInt($("#remark").val()))){
							return alert("短号必须为数字类型！");
							return false;
						}
					}
					if(!$("#password").val()){
						return alert("请输入密码！");
						return false;
					}
					if($("#repassword").val()!=$("#password").val()){
						return alert("两次密码不一致！");
						return false;
					}
					document.getElementById('login').submit();
				}
			</script>
		</body>
</html>