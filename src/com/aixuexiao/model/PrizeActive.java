package com.aixuexiao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 奖品类型实体
 */
public class PrizeActive implements Serializable {
	
	
	 /**
	 * 
	 */
	 
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private String des;
	private int score;
	private Date starttime;
	private Date endtime;
	private int qd;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public Date getStarttime() {
		return starttime;
	}
	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}
	public Date getEndtime() {
		return endtime;
	}
	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	public int getQd() {
		return qd;
	}
	public void setQd(int qd) {
		this.qd = qd;
	}
}
