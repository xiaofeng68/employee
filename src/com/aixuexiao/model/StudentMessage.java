package com.aixuexiao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 给员工的留言
 */
public class StudentMessage implements Serializable {
	
	
	 /**
	 * 
	 */
	 
	private static final long serialVersionUID = 1L;

	/**
	 * 留言编号
	 */
	private int id;
	
	/**
	 * 员工编号
	 */
	private int studentid;
	
	/**
	 * 公告内容
	 */
	private String content;
	
	/**
	 * 发布时间
	 */
	private Date inserttime;
	private Date updatetime;
	private String sname;
	private String cname;
	private String bkcontent;
	private int classid;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStudentid() {
		return studentid;
	}

	public void setStudentid(int studentid) {
		this.studentid = studentid;
	}
 
	

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getInserttime() {
		return inserttime;
	}

	public void setInserttime(Date inserttime) {
		this.inserttime = inserttime;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public int getClassid() {
		return classid;
	}

	public void setClassid(int classid) {
		this.classid = classid;
	}

	public String getBkcontent() {
		return bkcontent;
	}

	public void setBkcontent(String bkcontent) {
		this.bkcontent = bkcontent;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	
	
}
