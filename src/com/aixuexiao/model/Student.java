package com.aixuexiao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 员工实体
 */
public class Student implements Serializable {
	
	
	 /**
	 * 
	 */
	 
	private static final long serialVersionUID = 1L;

	/**
	 * 员工编号
	 */
	private int id;
	
	/**
	 * 班级编号
	 */
	private int classid;
	private String className;
	
	/**
	 * 员工姓名
	 */
	private String name;
	private String password;
	private String openid;
	private String phone;
	private Date updatetime;
	private String sex;
	
	/**
	 * 备注
	 */
	private String remark;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClassid() {
		return classid;
	}

	public void setClassid(int classid) {
		this.classid = classid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
}
