package com.aixuexiao.model;

import java.io.Serializable;

/**
 * 积分实体
 */
public class Score implements Serializable {
	
	
	 /**
	 * 
	 */
	 
	private static final long serialVersionUID = 1L;

	private int id;
	/**
	 * 用户id
	 */
	private int userid;
	/**
	 * 总积分
	 */
	private int score;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

}
