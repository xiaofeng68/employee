package com.aixuexiao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 奖品实体
 */
public class Prize implements Serializable {
	
	
	 /**
	 * 
	 */
	 
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private String des;
	private int num;
	private Date optiondate;
	/**
	 * 奖品类型id
	 */
	private int prizeactiveid;
	/**
	 * 奖品图片
	 */
	private String img;
	/**
	 * 
	 */
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDes() {
		return des;
	}
	public void setDes(String des) {
		this.des = des;
	}
	
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public Date getOptiondate() {
		return optiondate;
	}
	public void setOptiondate(Date optiondate) {
		this.optiondate = optiondate;
	}
	public int getPrizeactiveid() {
		return prizeactiveid;
	}
	public void setPrizeactiveid(int prizeactiveid) {
		this.prizeactiveid = prizeactiveid;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	
}
