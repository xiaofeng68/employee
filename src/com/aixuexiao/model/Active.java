package com.aixuexiao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 俱乐部活动
 */
public class Active implements Serializable {
	
	
	 /**
	 * 
	 */
	 
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;//活动名称
	private int clubid;//俱乐部
	private String clubname;//俱乐部名称
	private Date begintime;//开始时间
	private Date endtime;//结束时间
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getClubid() {
		return clubid;
	}
	public void setClubid(int clubid) {
		this.clubid = clubid;
	}
	public String getClubname() {
		return clubname;
	}
	public void setClubname(String clubname) {
		this.clubname = clubname;
	}
	public Date getBegintime() {
		return begintime;
	}
	public void setBegintime(Date begintime) {
		this.begintime = begintime;
	}
	public Date getEndtime() {
		return endtime;
	}
	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

}
