package com.aixuexiao.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 员工俱乐部关系
 */
public class StudentClub implements Serializable {
	
	
	 /**
	 * 
	 */
	 
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private int id;
	
	/**
	 * 员工编号
	 */
	private int studentid;
	
	/**
	 * 备注
	 */
	private String content;
	
	/**
	 * 状态
	 */
	private Date inserttime;
	private int status;
	private int clubid;
	private String studentname;
	private String clubname;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStudentid() {
		return studentid;
	}

	public void setStudentid(int studentid) {
		this.studentid = studentid;
	}
 
	

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getInserttime() {
		return inserttime;
	}

	public void setInserttime(Date inserttime) {
		this.inserttime = inserttime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getClubid() {
		return clubid;
	}

	public void setClubid(int clubid) {
		this.clubid = clubid;
	}

	public String getStudentname() {
		return studentname;
	}

	public void setStudentname(String studentname) {
		this.studentname = studentname;
	}

	public String getClubname() {
		return clubname;
	}

	public void setClubname(String clubname) {
		this.clubname = clubname;
	}
	
	
}
