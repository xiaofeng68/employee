package com.aixuexiao.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.aixuexiao.dao.ActiveDao;
import com.aixuexiao.model.Active;

@Service("activeService")
public class ActiveService {

	@Resource(name="activeDao")
	private ActiveDao activeDao;
	
	
	/**
	 * 将数据库中Active数据分页查出
	 * @param start 其实数据条数
	 * @param size  展示数据每页的大小
	 */
	public List<Active> listActive(int start,int size,Active active){
		return activeDao.findActive(start, size,active);
	}
	public int countActive(Active active){
		return activeDao.countActive(active);
	}
	/**
	 * 添加活动到数据库中
	 * @param active 活动对象
	 */
	public void addActive(Active active){
		activeDao.addActive(active);
	}
	/**   
	 * @Title: updateActive   
	 * @Description: 修改活动信息
	 * @param active
	 * @author  author
	 */
	 
	public void updateActive(Active active) {
		activeDao.updateActive(active);
	}
	
	/**
	 * 根据id查找对应的Active对象
	 * @param id 活动编号
	 * @return
	 */
	public Active findActiveById(int id){
		return activeDao.findActiveById(id);
	}
	
	public int deleteActiveById(int id){
		return activeDao.deleteActiveById(id);
	}
	
	/**
	 * 查询俱乐部最新还未开展的活动
	 * @param clubid
	 * @return
	 */
	public Active findActiveByClub(int clubid){
		return activeDao.findActiveByClub(clubid);
	}
}
