package com.aixuexiao.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.aixuexiao.dao.PrizeActiveDao;
import com.aixuexiao.model.PrizeActive;

@Service("prizeActiveService")
public class PrizeActiveService {

	@Resource(name="prizeActiveDao")
	private PrizeActiveDao prizeActiveDao;
	
	
	public List<PrizeActive> listPrizeActive(int start,int size,PrizeActive prizeActive){
		return prizeActiveDao.findPrizeActive(start,size,prizeActive);
	}
	public int countPrizeActive(PrizeActive prizeActive){
		return prizeActiveDao.countPrizeActive(prizeActive);
	}
	public PrizeActive findPrizeActiveById(int prizeActiveid){
		return prizeActiveDao.findPrizeActiveById(prizeActiveid);
	}
	public int addPrizeActive(PrizeActive prizeActive){
		return prizeActiveDao.addPrizeActive(prizeActive);
	}
	public int deletePrizeActiveById(int prizeActiveid) {
		return prizeActiveDao.deletePrizeActiveById(prizeActiveid);
	}
	public int updatePrizeActive(PrizeActive prizeActive) {
		return prizeActiveDao.updatePrizeActive(prizeActive);
	}
	public PrizeActive findOnPrizeActive(){
	    prizeActiveDao.updateQdState();
		return prizeActiveDao.findOnPrizeActive();
	}
}
