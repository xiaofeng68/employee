package com.aixuexiao.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.aixuexiao.dao.SettingDao;
import com.aixuexiao.model.Setting;

@Service("settingService")
public class SettingService {

	@Resource(name="settingDao")
	private SettingDao settingDao;
	
	
	
	public Setting isAdministrator(String name){
		return settingDao.isAdministrator(name);
	}
	public int updateAdministrator(Setting setting){
		return settingDao.updateAdministrator(setting);
	}
}
