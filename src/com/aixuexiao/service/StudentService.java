package com.aixuexiao.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.aixuexiao.dao.ClassesDao;
import com.aixuexiao.dao.ClubDao;
import com.aixuexiao.dao.ExamDao;
import com.aixuexiao.dao.StudentClubDao;
import com.aixuexiao.dao.StudentDao;
import com.aixuexiao.dao.StudentMessageDao;
import com.aixuexiao.model.Classes;
import com.aixuexiao.model.Club;
import com.aixuexiao.model.ExamMark;
import com.aixuexiao.model.Student;
import com.aixuexiao.model.StudentClub;
import com.aixuexiao.model.StudentMessage;

@Service("studentService")
public class StudentService {

	@Resource(name="studentDao")
	private StudentDao studentDao;
	
	@Resource(name="classesDao")
	private ClassesDao classesDao;
	@Resource(name="clubDao")
	private ClubDao clubDao;
	
	@Resource(name="studentMessageDao")
	private StudentMessageDao studentMessageDao;
	@Resource(name="studentClubDao")
	private StudentClubDao studentClubDao;
	
	
	@Resource(name="examDao")
	private ExamDao examDao;
	
	/**
	 * 根据员工编号查找对应的考试信息
	 * @param id 员工编号
	 * @param limit
	 * @return
	 */
	public List<ExamMark> findExamMarkByStudentId(int id ,int limit){
		return examDao.findExamMarkByStudentId(id, limit);
	}
	
	
	/**
	 * 将数据库中Student数据分页查出
	 * @param start 其实数据条数
	 * @param size  展示数据每页的大小
	 */
	public List<Student> listStudent(int start,int size,Student student){
		return studentDao.findStudent(start,size,student);
	}
	public int countStudent(Student student){
		return studentDao.countStudent(student);
	}
	public int findMaxId(){
		Integer maxId = studentDao.findMaxId();
		if(maxId!=null) return maxId;
		return 1;
	}
	/**
	 * 根据员工编号查找对应的员工
	 * @param studentid 员工编号
	 * @return 员工数据
	 */
	public Student findStudentById(int studentid){
		return studentDao.findStudentById(studentid);
	}
	public Student findStudentByOpenId(String openid){
		if(StringUtils.isEmpty(openid)) return null;
		return studentDao.findStudentByOpenId(openid);
	}
	/**   
	 * @Title: removeStudentOpenId   
	 * @Description: 解绑用户id   
	 * @param id
	 * @return
	 * @author  author
	 */
	 
	public int removeStudentOpenId(int id){
		return studentDao.removeStudentOpenId(id);
	}
	/**
	 * 添加老师给员工的微信留言到数据库
	 * @param studentMessage
	 */
	public void addStudentMessage(StudentMessage studentMessage){
		studentMessageDao.addStudentMessage(studentMessage);
	}
	public void addStudentClub(StudentClub studentClub){
		studentClubDao.addStudentClub(studentClub);
	}
	
	/**
	 * 根据id删除老师给员工的微信留言
	 * @param id
	 */
	public void deleteStudentMessageById(int id){
		studentMessageDao.deleteStudentMessageById(id);
	}
	
	/**
	 * 将数据库中Classes数据全部查出
	 */
	public List<Classes> findAllClasses(){
		return classesDao.findAllClasses();
	}
	public List<Club> findAllClub(){
		return clubDao.findAllClub();
	}
	public Club findClubById(int id){
		return clubDao.findClubById(id);
	}
	/**
	 * 根据员工id查询对应员工的留言记录
	 * @param studentid 员工id
	 * @param limit  展示数据每页的大小
	 */
	public List<StudentMessage> listMessageByStudentId(int studentid,int limit){
		return studentMessageDao.findStudentMessageByStudentId(studentid, limit);
	}
	
	
}
