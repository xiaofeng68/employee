package com.aixuexiao.service;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.aixuexiao.dao.PrizeActiveDao;
import com.aixuexiao.dao.PrizeDao;
import com.aixuexiao.dao.PrizeNoteDao;
import com.aixuexiao.dao.ScoreDao;
import com.aixuexiao.model.PrizeActive;
import com.aixuexiao.model.PrizeNote;
import com.aixuexiao.model.Score;

@Service("prizeNoteService")
public class PrizeNoteService {

	@Resource(name="prizeNoteDao")
	private PrizeNoteDao prizeNoteDao;
	@Resource(name="prizeActiveDao")
	private PrizeActiveDao prizeActiveDao;
	@Resource(name="scoreDao")
	private ScoreDao scoreDao;
	@Resource(name="prizeDao")
	private PrizeDao prizeDao;
	
	public List<PrizeNote> listPrizeNote(int start,int size,PrizeNote prizeNote){
		return prizeNoteDao.findPrizeNote(start,size,prizeNote);
	}
	public int countPrizeNote(PrizeNote prizeNote){
		return prizeNoteDao.countPrizeNote(prizeNote);
	}
	public PrizeNote findPrizeNoteById(int prizeNoteid){
		return prizeNoteDao.findPrizeNoteById(prizeNoteid);
	}
	public int addPrizeNote(PrizeNote prizeNote){
		return prizeNoteDao.addPrizeNote(prizeNote);
	}
	public int deletePrizeNoteById(int prizeNoteid) {
		return prizeNoteDao.deletePrizeNoteById(prizeNoteid);
	}
	/**
	 * 用户是否已抽奖
	 * @param userid
	 * @return
	 */
	public boolean isPrizeNoteByUserid(int userid){
		//查询出活动起止时间段
		//根据时间段查询出用户记录
		int count = prizeNoteDao.isPrizeNoteByUserid(userid);
		//如果记录》0返回true
		return count>0;
	}
	public synchronized boolean savePrizeDetail(int userid,int prizeid,int paid){
		//查询活动信息
		PrizeActive prizeActive = prizeActiveDao.findPrizeActiveById(paid);
		//保存抽奖信息
		PrizeNote prizeNote = new PrizeNote();
		prizeNote.setPrizeid(prizeid);
		prizeNote.setDay(new Date());
		prizeNote.setScore(prizeActive.getScore());
		prizeNote.setPrizeactiveid(paid);
		prizeNote.setUserid(userid);
		prizeNoteDao.addPrizeNote(prizeNote);
		//更新用户积分
		Score score = new Score();
		score.setUserid(userid);
		score.setScore(prizeActive.getScore());
		scoreDao.updateScoreM(score);
		//减去奖品数量--1
		prizeDao.updatePrizeNum(prizeid);
		return true;
	}
}
