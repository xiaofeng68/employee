package com.aixuexiao.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.aixuexiao.dao.ScoreDao;
import com.aixuexiao.model.Score;

@Service("scoreService")
public class ScoreService {

	@Resource(name="scoreDao")
	private ScoreDao scoreDao;
	
	
	public List<Score> listScore(int start,int size,Score score){
		return scoreDao.findScore(start,size,score);
	}
	public int countScore(Score score){
		return scoreDao.countScore(score);
	}
	public Score findScoreById(int scoreid){
		return scoreDao.findScoreById(scoreid);
	}
	public Score findScoreByUserId(int userid){
		return scoreDao.findScoreByUserId(userid);
	}
	public int addScore(Score score){
		return scoreDao.addScore(score);
	}
	public int deleteScoreById(int scoreid) {
		return scoreDao.deleteScoreById(scoreid);
	}
	public int updateScore(Score score) {
		return scoreDao.updateScore(score);
	}
	public int getScoreById(int userid){
		return scoreDao.getScoreByUserid(userid);
	}
	/**
	 * 根据用户签到天数返回积分数量
	 * @param userid
	 * @return
	 */
	public int getScoreRule(int userid){
	    return scoreDao.getLastScore(userid);
	}
}
