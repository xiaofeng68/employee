package com.aixuexiao.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.aixuexiao.dao.ClubDao;
import com.aixuexiao.dao.StudentDao;
import com.aixuexiao.model.Club;
import com.aixuexiao.model.Student;

@Service("clubService")
public class ClubService {

	@Resource(name="studentDao")
	private StudentDao studentDao;
	
	@Resource(name="clubDao")
	private ClubDao clubDao;
	
	
	/**
	 * 将数据库中Club数据分页查出
	 * @param start 其实数据条数
	 * @param size  展示数据每页的大小
	 */
	public List<Club> listClub(int start,int size,Club club){
		return clubDao.findClub(start, size,club);
	}
	public int countClub(Club club){
		return clubDao.countClub(club);
	}
	/**
	 * 添加班级到数据库中
	 * @param club 班级对象
	 */
	public void addClub(Club club){
		clubDao.addClub(club);
	}
	/**   
	 * @Title: updateClub   
	 * @Description: 修改公司信息
	 * @param club
	 * @author  author
	 */
	 
	public void updateClub(Club club) {
		clubDao.updateClub(club);
	}
	/**
	 * 删除数据库中对应id的公司信息
	 * @param clubId 公司id
	 */
	public void clubchangestate(int clubId,int status){
		clubDao.clubchangestate(clubId,status);
	}
	/**
	 * 根据id查找对应的Club对象
	 * @param id 班级编号
	 * @return
	 */
	public Club findClubById(int id){
		return clubDao.findClubById(id);
	}
	
	/**
	 * 根据班级id查找对应班级所有员工
	 * @param clubid 班级id
	 * @return
	 */
	public List<Student> findStudentByClubId(int clubid){
		return studentDao.findStudentByClubId(clubid);
	}
	
	public List<Club> findAllClub(){
		return clubDao.findAllClub();
	}
}
