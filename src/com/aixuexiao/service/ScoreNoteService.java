package com.aixuexiao.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.aixuexiao.dao.ScoreNoteDao;
import com.aixuexiao.model.ScoreNote;

@Service("scoreNoteService")
public class ScoreNoteService {

	@Resource(name="scoreNoteDao")
	private ScoreNoteDao scoreNoteDao;
	
	
	public List<ScoreNote> listScoreNote(int start,int size,ScoreNote scoreNote){
		return scoreNoteDao.findScoreNote(start,size,scoreNote);
	}
	public int countScoreNote(ScoreNote scoreNote){
		return scoreNoteDao.countScoreNote(scoreNote);
	}
	public ScoreNote findScoreNoteById(int scoreNoteid){
		return scoreNoteDao.findScoreNoteById(scoreNoteid);
	}
	public int addScoreNote(ScoreNote scoreNote){
		return scoreNoteDao.addScoreNote(scoreNote);
	}
	public int deleteScoreNoteById(int scoreNoteid) {
		return scoreNoteDao.deleteScoreNoteById(scoreNoteid);
	}
	public List<Map<String,Object>> getMonthScoreNoteById(int userid,Date date){
		List<Map<String,Object>> list = scoreNoteDao.getMonthScoreNoteById(userid,date);
		if(list==null) list = new ArrayList<Map<String,Object>>();
		return list;
	}
	public boolean isTodayScore(int userid){
		try {
			return scoreNoteDao.isTodayScore(userid);
		} catch (Exception e) {
			return false;
		}
	}
}
