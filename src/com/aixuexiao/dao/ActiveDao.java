package com.aixuexiao.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.aixuexiao.model.Active;

@Component("activeDao")
public class ActiveDao extends BaseDao {
	
	public List<Active> findActive(int start,int size,Active active){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("start", start);
		map.put("size", size);
		map.put("active", active);
		return this.readSqlSession.selectList("com.aixuexiao.dao.ActiveDao.selectActive",map);
	}
	public int countActive(Active active){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("active", active);
		return this.readSqlSession.selectOne("com.aixuexiao.dao.ActiveDao.selectActiveCount",map);
	}
	public void addActive(Active active){
		writerSqlSession.insert("com.aixuexiao.dao.ActiveDao.addActive", active);
	}
	public void updateActive(Active active){
		writerSqlSession.update("com.aixuexiao.dao.ActiveDao.updateActive", active);
	}
	public Active findActiveById(int id){
		return this.readSqlSession.selectOne("com.aixuexiao.dao.ActiveDao.selectActiveById",id);
	}
	public int deleteActiveById(int id) {
		return this.writerSqlSession.delete("com.aixuexiao.dao.ActiveDao.deleteActiveById", id);
	}
	public Active findActiveByClub(int clubid){
		return this.readSqlSession.selectOne("com.aixuexiao.dao.ActiveDao.selectActiveByClub",clubid);
	}
}
