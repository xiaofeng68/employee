package com.aixuexiao.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.aixuexiao.model.StudentClub;

@Component("studentClubDao")
public class StudentClubDao extends BaseDao {


	public List<StudentClub> selectStudentClub(int start,int size,StudentClub studentClub) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("start", start);
		map.put("size", size);
		map.put("student", studentClub);
		return this.readSqlSession.selectList("com.aixuexiao.dao.StudentClubDao.selectStudentClub",map);
	}
	public int count(StudentClub studentClub) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("student", studentClub);
		return this.readSqlSession.selectOne("com.aixuexiao.dao.StudentClubDao.selectCount",map);
	}
	
	public int addStudentClub(StudentClub studentClub) {
		return this.writerSqlSession.insert("com.aixuexiao.dao.StudentClubDao.addStudentClub", studentClub);
	}
	public int updateStudentClubStatus(int id) {
		return this.writerSqlSession.update("com.aixuexiao.dao.StudentClubDao.updateStudentClubStatus", id);
	}
	
	public int deleteStudentClubById(int id) {
		return this.writerSqlSession.delete("com.aixuexiao.dao.StudentClubDao.deleteStudentClubById", id);
	}
	public void studentclubchangestate(int clubId,int status){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("id", clubId);
		map.put("status", status);
		writerSqlSession.update("com.aixuexiao.dao.StudentClubDao.studentclubchangestate", map);
	}
	public StudentClub findStudentClubCount(int stuid,int clubid){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("studentid", stuid);
		map.put("clubid", clubid);
		return writerSqlSession.selectOne("com.aixuexiao.dao.StudentClubDao.selectCountByStudentClub", map);
	}
}
