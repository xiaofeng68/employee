package com.aixuexiao.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.aixuexiao.model.PrizeActive;

@Component("prizeActiveDao")
public class PrizeActiveDao extends BaseDao {


	public PrizeActive findPrizeActiveById(int id) {
		return this.readSqlSession.selectOne("com.aixuexiao.dao.PrizeActiveDao.selectPrizeActiveById",id);
	}
	public List<PrizeActive> findPrizeActive(int start,int size,PrizeActive prizeActive) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("start", start);
		map.put("size", size);
		map.put("prizeActive", prizeActive);
		return this.readSqlSession.selectList("com.aixuexiao.dao.PrizeActiveDao.selectPrizeActive",map);
	}
	public int countPrizeActive(PrizeActive prizeActive) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("prizeActive", prizeActive);
		return this.readSqlSession.selectOne("com.aixuexiao.dao.PrizeActiveDao.selectPrizeActiveCount",map);
	}
	
	public int addPrizeActive(PrizeActive prizeActive) {
		return this.writerSqlSession.insert("com.aixuexiao.dao.PrizeActiveDao.addPrizeActive", prizeActive);
	}
	
	public int deletePrizeActiveById(int prizeActiveid) {
		return this.writerSqlSession.delete("com.aixuexiao.dao.PrizeActiveDao.deletePrizeActiveById", prizeActiveid);
	}
	
	public int updatePrizeActive(PrizeActive prizeActive) {
		return this.writerSqlSession.update("com.aixuexiao.dao.PrizeActiveDao.updatePrizeActive", prizeActive);
	}
	public PrizeActive findPrizeActiveBypid(int prizeid){
		return this.readSqlSession.selectOne("com.aixuexiao.dao.PrizeActiveDao.selectPrizeActiveBypid",prizeid);
	}
	public PrizeActive findOnPrizeActive(){
		return this.readSqlSession.selectOne("com.aixuexiao.dao.PrizeActiveDao.selectOnPrizeActive");
	}
	public void updateQdState(){
	    this.writerSqlSession.update("com.aixuexiao.dao.PrizeActiveDao.updateQdState");
	}
}
