package com.aixuexiao.web.controller;


import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.aixuexiao.model.Classes;
import com.aixuexiao.model.ClassesNews;
import com.aixuexiao.model.Grid;
import com.aixuexiao.model.Student;
import com.aixuexiao.service.ClassesService;
import com.aixuexiao.service.StudentService;

/**
 * 包含公司列表菜单内的所有操作
 */
@Controller
public class ClassesController {
	
	
	public static int pagesize = 8;
	
	@Resource(name="classesService")
	private ClassesService classesService;
	@Resource(name="studentService")
	private StudentService studentService;
	
	@RequestMapping(value="/manager/classes")
	public ModelAndView listStudent(String pagenum,HttpServletRequest request){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("classes");
		mv.addObject("sidebar","classes");
		int num = 1;
		if(null!=pagenum && !StringUtils.isEmpty(pagenum)){
			num = Integer.parseInt(pagenum);
		}
		Classes classes = new Classes();
		classes.setName(request.getParameter("name"));
		List<Classes> list = classesService.listClasses((num-1)*pagesize, pagesize,classes);
		int count = classesService.countClasses(classes);
		Grid<Classes> grid = new Grid<Classes>();
		grid.setRows(list);
		grid.setTotal(count);
		grid.setCurrentPage(num);
		grid.setPageNum(pagesize);
		mv.addObject("grid",grid);
		mv.addObject("classes", classes);
		return mv;
	}

	
	@RequestMapping(value="/manager/addclassespage",method=RequestMethod.GET)
	public ModelAndView addClassesPage(){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("addclasses");
		mv.addObject("sidebar","classes");
		return mv;
	}
	
	@RequestMapping(value="/manager/addclasses",method=RequestMethod.POST)
	public ModelAndView addClasses(Classes classes){
		ModelAndView mv=new ModelAndView();
		Classes cls = classesService.findClassesById(classes.getId());
		if(null==cls){
			mv.setViewName("redirect:/manager/classes");
			classes.setStudentcount(0);
			classesService.addClasses(classes);
		}else{
			mv.setViewName("redirect:/manager/addclassespage");
			mv.addObject("name", classes.getName());
			mv.addObject("headteacher", classes.getHeadteacher());
			mv.addObject("notice","已存在编号为"+classes.getId()+"的公司");
		}
		return mv;
	}
	@RequestMapping(value="/manager/updateclasses",method=RequestMethod.POST)
	public ModelAndView updateclasses(Classes classes){
		ModelAndView mv=new ModelAndView();
		classesService.updateClasses(classes); 
		mv.addObject("notice","编辑公司信息成功");
		mv.setViewName("redirect:/manager/classes");
		return mv;
	}
	
	@RequestMapping(value="/manager/deleteclasses",method=RequestMethod.GET)
	public ModelAndView deleteclasses(int id){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("redirect:/manager/classes");
		classesService.deleteclasses(id);
		mv.addObject("notice","删除公司信息成功");
		return mv;
	}
	
	@RequestMapping(value="/manager/managerstudentpage",method=RequestMethod.GET)
	public ModelAndView studentPage(String pagenum,int classesid){
		ModelAndView mv=new ModelAndView();
		Classes cls = classesService.findClassesById(classesid);
		int num = 1;
		if(null!=pagenum){
			num = Integer.parseInt(pagenum);
		}
		Student student = new Student();
		student.setClassid(classesid);
		List<Student> stlist = studentService.listStudent((num-1)*pagesize, pagesize,student);
		mv.setViewName("addstudents");
		mv.addObject("sidebar","classes");
		mv.addObject("cls",cls);
		mv.addObject("stlist",stlist);
		mv.addObject("maxId",studentService.findMaxId());
		mv.addObject("length", stlist.size());
		mv.addObject("pagenum", num);
		return mv;
	}

	
	@RequestMapping(value="/manager/classesnewspage",method=RequestMethod.GET)
	public ModelAndView classesnewsPage(int classesid){
		ModelAndView mv=new ModelAndView();
		Classes cls = classesService.findClassesById(classesid);
		List<ClassesNews> cnlist= classesService.findClassesNewsByClassId(classesid);
		mv.setViewName("addclassesnews");
		mv.addObject("sidebar","classes");
		mv.addObject("cls",cls);
		mv.addObject("cnlist",cnlist);
		return mv;
	}
	
	@RequestMapping(value="/manager/addclassesnews",method=RequestMethod.POST)
	public ModelAndView addClassesNews(ClassesNews classesNews){
		ModelAndView mv=new ModelAndView();
		classesNews.setInserttime(new Date());
		classesService.addClassesNews(classesNews);
		mv.addObject("notice","添加公司动态成功");
		mv.addObject("classesid",classesNews.getClassid());
		mv.setViewName("redirect:/manager/classesnewspage");
		return mv;
	}
	
	
	@RequestMapping(value="/manager/deleteclassesnews",method=RequestMethod.GET)
	public ModelAndView deleteClassesNews(int classesid,int id){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("redirect:/manager/classesnewspage");
		mv.addObject("classesid",classesid);
		classesService.deleteClassesNewsById(id);
		mv.addObject("notice","删除动态成功");
		return mv;
	}
	
	
	@RequestMapping(value="/manager/addstudent",method=RequestMethod.POST)
	public ModelAndView addStudent(Student student,String viewName){
		ModelAndView mv=new ModelAndView();
		int id = student.getId();
		Student stu = id==0?null:classesService.findStudentById(student.getId());
		if(stu==null){
			if(id ==0){
				student.setId(studentService.findMaxId());
			}
			classesService.addStudent(student);
			classesService.updateClassStudentCount(student.getClassid());
			mv.addObject("notice","添加员工成功");
		}else{
			mv.addObject("notice","已经存在编号为"+student.getId()+"的员工("+stu.getName()+")！");
		}
		mv.addObject("classesid",student.getClassid());
		if(StringUtils.isEmpty(viewName)){
			viewName = "/manager/managerstudentpage";
		}
		mv.setViewName("redirect:"+viewName);
		return mv;
	}
	
	@RequestMapping(value="/manager/deletestudent",method=RequestMethod.GET)
	public ModelAndView deleteStudent(int studentid,int classid,@RequestParam(value="type", defaultValue="0") int type){
		ModelAndView mv=new ModelAndView();
		classesService.deleteStudentById(studentid);
		classesService.updateClassStudentCount(classid);
		mv.addObject("classesid",classid);
		mv.addObject("notice","删除员工信息成功");
		String viewName="/manager/managerstudentpage";
		if(type==1){
			viewName = "/manager/students";
		}
		mv.setViewName("redirect:"+viewName);
		return mv;
	}
	
	@RequestMapping(value="/manager/updatestudent",method=RequestMethod.POST)
	public ModelAndView updateStudent(Student student,String viewName){
		ModelAndView mv=new ModelAndView();
		classesService.updateStudentBy(student);
		mv.addObject("notice","编辑员工信息成功");
		mv.addObject("classesid",student.getClassid());
		if(StringUtils.isEmpty(viewName)){
			viewName = "/manager/managerstudentpage";
		}
		mv.setViewName("redirect:"+viewName);
		return mv;
	}
	
}
