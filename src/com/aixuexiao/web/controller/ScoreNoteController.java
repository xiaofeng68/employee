package com.aixuexiao.web.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.aixuexiao.model.Classes;
import com.aixuexiao.model.Grid;
import com.aixuexiao.model.ScoreNote;
import com.aixuexiao.service.ScoreNoteService;
import com.aixuexiao.service.StudentService;

/**
 * 用户积分记录的所有操作
 */
@Controller()
public class ScoreNoteController {
	public static final int pagesize = 8;
	@Resource(name="studentService")
    private StudentService studentService;
	@Resource(name="scoreNoteService")
	private ScoreNoteService scoreNoteService;
	
	
	@RequestMapping(value="/manager/scoreNotes")
	public ModelAndView listScoreNote(String pagenum,HttpServletRequest request){
		ScoreNote scoreNote = new ScoreNote();
		scoreNote.setSname(request.getParameter("sname"));
        String classid = request.getParameter("classid");
        if(!StringUtils.isEmpty(classid))
            scoreNote.setClassid(Integer.parseInt(classid));
        String userid = request.getParameter("userid");
        if(!StringUtils.isEmpty(userid))
            scoreNote.setUserid(Integer.parseInt(userid));
		
		ModelAndView mv=new ModelAndView();
		mv.setViewName("scoreNotes");
		mv.addObject("sidebar","scoreNotes");
		int num = 1;
		if(null!=pagenum && !StringUtils.isEmpty(pagenum)){
			num = Integer.parseInt(pagenum);
		}
		List<ScoreNote> list = scoreNoteService.listScoreNote((num-1)*pagesize, pagesize,scoreNote);
		int count = scoreNoteService.countScoreNote(scoreNote);
		Grid<ScoreNote> grid = new Grid<ScoreNote>();
		grid.setRows(list);
        grid.setTotal(count);
        grid.setCurrentPage(num);
        grid.setPageNum(pagesize);
        mv.addObject("grid",grid);
        mv.addObject("scoreNote", scoreNote);
		List<Classes> clslist = studentService.findAllClasses();
        mv.addObject("clsList", clslist);
		return mv;
	}
}
