package com.aixuexiao.web.controller;


import java.io.File;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.aixuexiao.model.Grid;
import com.aixuexiao.model.Prize;
import com.aixuexiao.model.PrizeActive;
import com.aixuexiao.service.PrizeActiveService;
import com.aixuexiao.service.PrizeService;
import com.aixuexiao.util.DateUtils;
import com.aixuexiao.util.UploadFileUtils;

@Controller
public class PrizeActiveController {
	
	
	public static int pagesize = 10;
	@Resource(name="prizeActiveService")
	private PrizeActiveService prizeActiveService;
	@Resource(name="prizeService")
	private PrizeService prizeService;
	
	@RequestMapping(value="/manager/prizeActive")
	public ModelAndView listPrizeActive(String pagenum,HttpServletRequest request){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("prizeActive");
		mv.addObject("sidebar","prizeActive");
		int num = 1;
		if(null!=pagenum && !StringUtils.isEmpty(pagenum)){
			num = Integer.parseInt(pagenum);
		}
		PrizeActive prizeActive = new PrizeActive();
		prizeActive.setName(request.getParameter("name"));
		List<PrizeActive> list = prizeActiveService.listPrizeActive((num-1)*pagesize, pagesize,prizeActive);
		int count = prizeActiveService.countPrizeActive(prizeActive);
		Grid<PrizeActive> grid = new Grid<PrizeActive>();
		grid.setRows(list);
		grid.setTotal(count);
		grid.setCurrentPage(num);
		grid.setPageNum(pagesize);
		mv.addObject("grid",grid);
		mv.addObject("prizeActive", prizeActive);
		return mv;
	}

	
	@RequestMapping(value="/manager/addPrizeActivepage",method=RequestMethod.GET)
	public ModelAndView addPrizeActivePage(){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("addPrizeActive");
		mv.addObject("sidebar","prizeActive");
		return mv;
	}
	
	@RequestMapping(value="/manager/addPrizeActive",method=RequestMethod.POST)
	public ModelAndView addPrizeActive(PrizeActive prizeActive,String startTime,String endTime){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("redirect:/manager/prizeActive");
		prizeActive.setStarttime(DateUtils.strToDate(startTime, DateUtils.TIME_FORMAT));
		prizeActive.setEndtime(DateUtils.strToDate(endTime, DateUtils.TIME_FORMAT));
		prizeActiveService.addPrizeActive(prizeActive);
		return mv;
	}
	@RequestMapping(value="/manager/prizeManagerForActivePage",method=RequestMethod.GET)
	public ModelAndView prizeManagerForActivePage(HttpServletRequest request){
		ModelAndView mv=new ModelAndView();
		mv.setViewName("prizeManagerForActive");
		int activeid = Integer.parseInt(request.getParameter("id"));
		mv.addObject("activeid",activeid);
		//根据activeid获取奖品列表
		Prize prize = new Prize();
		prize.setPrizeactiveid(activeid);
		List<Prize> prizeList = prizeService.listPrize(0, 4, prize);
		mv.addObject("prizeList",prizeList);
		mv.addObject("sidebar","prizeActive");
		return mv;
	}
	
	@SuppressWarnings("deprecation")
	@RequestMapping(value="/manager/updateprize",method=RequestMethod.POST)
	public ModelAndView updateprize(Prize prize,@RequestParam MultipartFile file,HttpServletRequest request){
		ModelAndView mv=new ModelAndView();
		//上传文件
		try {
			String fileName = file.getOriginalFilename();
			String suffix = fileName.substring(fileName.lastIndexOf("."));  
	        //随机生成一个文件名
			fileName = UploadFileUtils.getRandomName(fileName,suffix);
			UploadFileUtils.upload4Stream(fileName, request.getRealPath(UploadFileUtils.path), file.getInputStream());
			prize.setImg(UploadFileUtils.path+File.separator+fileName);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		prize.setOptiondate(new Date());
    	prizeService.updatePrize(prize);
		mv.addObject("notice","编辑抽奖活动信息成功");
		mv.setViewName("prizeManagerForActive");
		int activeId = prize.getPrizeactiveid();
		//根据activeid获取奖品列表
		prize = new Prize();
		prize.setPrizeactiveid(activeId);
		List<Prize> prizeList = prizeService.listPrize(0, 4, prize);
		mv.addObject("prizeList",prizeList);
		mv.addObject("sidebar","prizeActive");
		return mv;
	}
	@SuppressWarnings("deprecation")
	@RequestMapping(value="/manager/addprize",method=RequestMethod.POST)
	public ModelAndView addprize(Prize prize,@RequestParam MultipartFile file,HttpServletRequest request){
		ModelAndView mv=new ModelAndView();
		String fileName = file.getOriginalFilename();
		String suffix = fileName.substring(fileName.lastIndexOf("."));  
        //随机生成一个文件名
		fileName = UploadFileUtils.getRandomName(fileName,suffix);
		//上传文件
		try {
			UploadFileUtils.upload4Stream(fileName, request.getRealPath(UploadFileUtils.path), file.getInputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
		prize.setOptiondate(new Date());
    	prize.setImg(UploadFileUtils.path+File.separator+fileName);	
    	prizeService.addPrize(prize);
		mv.addObject("notice","添加抽奖活动信息成功");
		mv.setViewName("prizeManagerForActive");
		int activeId = prize.getPrizeactiveid();
		//根据activeid获取奖品列表
		prize = new Prize();
		prize.setPrizeactiveid(activeId);
		List<Prize> prizeList = prizeService.listPrize(0, 4, prize);
		mv.addObject("prizeList",prizeList);
		mv.addObject("sidebar","prizeActive");
		return mv;
	}
	@RequestMapping(value="/manager/updatePrizeActive",method=RequestMethod.POST)
	public ModelAndView updatePrizeActive(PrizeActive prizeActive,String startTime,String endTime){
		ModelAndView mv=new ModelAndView();
		prizeActive.setStarttime(DateUtils.strToDate(startTime, DateUtils.TIME_FORMAT));
		prizeActive.setEndtime(DateUtils.strToDate(endTime, DateUtils.TIME_FORMAT));
		prizeActiveService.updatePrizeActive(prizeActive); 
		mv.addObject("notice","编辑抽奖活动信息成功");
		mv.setViewName("redirect:/manager/prizeActive");
		return mv;
	}
	@RequestMapping(value="/manager/deletePrizeActive",method=RequestMethod.GET)
	public ModelAndView deleteStudentMessage(int id){
		ModelAndView mv=new ModelAndView();
		prizeActiveService.deletePrizeActiveById(id);
		mv.addObject("notice","删除抽奖活动信息成功");
		mv.setViewName("redirect:/manager/prizeActive");
		return mv;
	}
	
	
}
